package org.naic;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Test;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

public class PetStoreTest {

    private static String baseURI = "https://petstore.swagger.io/v2/pet/";

    @Test
    public void findPetsByStatusTest() {
        String petStoreURI = baseURI + "findByStatus?";

        RestAssured.baseURI =petStoreURI;
		RequestSpecification request = RestAssured.given();

        Response response = request.queryParam("status", "available").get();

        assertEquals(200, response.getStatusCode());

        int petResponseSize = response.getBody().jsonPath().getList("$").size();
        assertTrue(petResponseSize > 1);
        String jsonString = response.prettyPrint();
        System.out.println("The response body: " +jsonString);

    }
}
