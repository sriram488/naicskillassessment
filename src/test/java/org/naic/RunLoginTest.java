package org.naic;

import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.Cucumber;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(
features = "src/test/resources/login.feature",dryRun=true, monochrome = true, plugin = {"pretty", "html:target/cucumber/", "json:target/report/cucumber.json"})
public class RunLoginTest { }
