package org.naic;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.annotation.en.Given;
import cucumber.annotation.en.Then;
import cucumber.annotation.en.When;

public class login {
   WebDriver driver = new ChromeDriver();
   @Given("^I am on NAIC login page$")

   public void goToNAICLogin() {
      driver.navigate().to("https://eapps.naic.org/lhub/");
      driver.findElement(By.id("cui-login")).click();
   }

   @When("^I enter username as \"(.*)\"$")
   public void enterUsername(String arg1) {
      driver.findElement(By.id("INT_USERNAME")).sendKeys(arg1);
   }

   @When ("^I enter password as \"(.*)\"$")
   public void enterPassword(String arg1) {
      driver.findElement(By.id("INT_PASSWORD")).sendKeys(arg1);
      driver.findElement(By.id("u_0_v")).click();
   }

   @Then("^Login should fail$")
   public void checkFail() {
      if(driver.getCurrentUrl().equalsIgnoreCase(
         "https://login.common.naic.org/nidp/app/login?id=naic&sid=1&option=credential&sid=1")){
            System.out.println("Test1 Pass");
      } else {
         System.out.println("Test1 Failed");
      }
      driver.close();
   }
}
