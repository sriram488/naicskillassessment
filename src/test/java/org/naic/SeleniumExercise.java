package org.naic;
import java.util.List;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SeleniumExercise {
	@Test
	public void main() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\sriram\\Desktop\\Selenium\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		String url = "http://www.naic.org";
		driver.get(url);
		driver.getTitle();
		System.out.println(driver.getTitle());
		driver.manage().window().maximize();
		WebElement myLinksWithId = driver.findElement(By.id("super_menu"));
		List<WebElement> childElements = myLinksWithId.findElements(By.tagName("href"));
		// Ensures there are six links within the above element
		assertEquals(6, childElements.size());

		 for(WebElement link:childElements)
		 {
 			// Check all href links start with https://www.naic.org
		 	boolean linkPrefix = link.getAttribute("href").startsWith("https://www.naic.org");
 			// if assert fails assert exception is caught and the links are printed.
			try{
				assertTrue(linkPrefix);
			}catch(AssertionError error) {
				System.out.println(" The links that does start with naic.org are: " + link.getAttribute("href"));
			}
		 }
		driver.close();
	}
}